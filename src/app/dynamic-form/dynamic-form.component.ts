import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuestionBase } from '../model/question-base';
import { FieldControlService } from '../service/field-control.service';
import { FormField , Userinfo} from '../model';
import { Constants } from '../common/constants';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [ FieldControlService ]
})
export class DynamicFormComponent implements OnInit {

  @Input() usersinfo: Userinfo[];

  @Input() fields: any[];
  @Input() menuCategories: any[];
  @Input() dropdownOptions: any[];

  @Output() updateMenu = new EventEmitter<any>();

  selectedCategory: any[];
  selectedFields: any[];

  form: FormGroup;
  payLoad = '';

  constructor(private fieldService: FieldControlService) { }

  ngOnInit() {
    // set default participant
    this.onChangeParticipant(Constants.PARTICIPANT_ID);
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.form.value);
  }

  onChangeParticipant(participantId) {

    let fieldsSelected: any[];
    let categorySelected: any[];

    this.menuCategories.forEach(element => {
      if (participantId.toString() === element.participantTypeId.toString()) {
        categorySelected = element;
      }
    });


    this.fields.forEach(element => {
      if (participantId.toString() === element.participantTypeId.toString()) {
        fieldsSelected = element;
      }
    });

    this.selectedCategory = categorySelected;
    this.selectedFields = fieldsSelected;

    this.form = this.fieldService.toFormGroup(fieldsSelected);
    this.updateMenu.emit({menuSelected: categorySelected});
  }
}
