import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuestionBase } from '../model/question-base';

@Component({
  selector: 'app-field',
  templateUrl: './dynamic-form-field.component.html'
})
export class DynamicFormFieldComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() field: any;
  @Input() dropdownOptions: any;

  options: any[] = [];

  constructor() {
  }

  ngOnInit() {

    // if it is a dropdown field then load option for dropdown
    this.loadOptionsDropdown();
  }

  loadOptionsDropdown() {

    if (this.field.controlType === 'dropdownBox' || this.field.controlType === 'multiSelectBox') {
      // this.options = this.getOptionsByFieldId(this.field.fieldId);
      this.options = this.getOptionsByFieldId('15');
    }
  }

  getOptionsByFieldId(fieldId: string) {
    const dropdownOption =  this.dropdownOptions.find((element) => {
      if (element.fieldId.toString() === fieldId.toString()) {
        return element;
      }
    });

    if (dropdownOption === undefined) {
      return [];
    }else {
      return dropdownOption.options;
    }
  }

  get isValid() { return this.form.controls[this.field.fieldId].valid; }
  // get isValid() { return {data: true}; }

}
