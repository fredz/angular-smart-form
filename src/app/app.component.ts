import { Component, OnInit  } from '@angular/core';

import { FieldService } from './service/field.service';
import { FormField, Userinfo } from './model';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Constants } from './common/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:  [FieldService]
})
export class AppComponent implements OnInit {

  usersinfo: Userinfo[];
  fields: any;
  menuCategories: any;
  dropdownOptions: any[];

  selectedCategory: any[] = [];

  constructor(private service: FieldService) {
  }

  ngOnInit() {

    forkJoin([
      this.service.getFormFields(),
      this.service.getMenu(),
      this.service.getUsersInfo(),
      this.service.getStates()
    ])
      .subscribe(results => {
        this.fields = results[0];
        this.menuCategories = results[1];
        this.usersinfo = results[2];
        this.dropdownOptions = results[3];

        this.defaultMenu();
      });

  }

  defaultMenu() {
    this.menuCategories.forEach(element => {
      if (Constants.PARTICIPANT_ID.toString() === element.participantTypeId.toString()) {
        this.selectedCategory = element.data;
      }
    });
  }

  handleUpdateMenu(event: any) {

    console.log(event.menuSelected.data);

    this.selectedCategory = event.menuSelected.data;
  }

}
