import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { QuestionBase } from '../model/question-base';
import { FormField } from '../model';
import { Constants } from '../common/constants';

@Injectable()
export class FieldControlService {

  constructor() { }

    toFormGroup(formFields: any ) {

      const group: any = {};
      formFields.data.forEach(field => {
        group[field.fieldId] = field.required ? new FormControl(field.fieldValue || '', Validators.required)
                                                  : new FormControl(field.fieldValue || '');
      });

      return new FormGroup(group);
    }

}
