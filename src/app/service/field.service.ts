import { Injectable } from '@angular/core';

import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { QuestionDropdown } from '../model/question-dropdown';
import { QuestionBase } from '../model/question-base';
import { QuestionTextbox } from '../model/question-textbox';
import { FormField, Userinfo } from '../model';

@Injectable()
export class FieldService {

  constructor(private http: Http) {

  }

  public getUsersInfo(): Observable<any> {
    return this.http.get('assets/data/users.json')
                    .map((res: any) => res.json());
  }


  public getFormFields(): Observable<any> {
    return this.http.get('assets/data/populate-form-val.json')
                    .map((res: any) => res.json());
  }


  public getMenu(): Observable<any> {
    return this.http.get('assets/data/menu.json')
                    .map((res: any) => res.json());
  }

  public getStates(): Observable<any> {
    return this.http.get('assets/data/stateData.json')
                    .map((res: any) => res.json());
  }

}
