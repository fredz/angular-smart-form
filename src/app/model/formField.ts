import { QuestionBase } from './question-base';

export interface FormField {
  title: string;
  questions: QuestionBase<any>[];
}

